package practica4Curs2;

public class act2App {

	public static void main(String[] args) {
		
		//Aqu� crear� variables amb valor escollit per mi
		
		int N = 3;
		double A = 4.5;
		char C = 'C';
		
		//Aqu� far� les operacions del exercici i ho mostrar� per consola
		System.out.println("Variable N: " + N);
		System.out.println("Variable A: " + A);
		System.out.println("Variable C: " + C);
		System.out.println("3 + 4.5 = " + (N + A));
		System.out.println("4.5 - 3 = " + (N - A));
		//Aqu� podem veure quin valor ascii t� la lletra que hem escollit
		System.out.println("Valor n�merico del car�cter C = " + (int)C);

	}

}
